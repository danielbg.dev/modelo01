<?php 
class AuthenticationRequest {
    protected $username;
    protected $password;

    public function __construct($username, $password){
        $this->username = $username;
        $this->password = $password;
    }

    public function isValid(){
        return true;
    }    
}