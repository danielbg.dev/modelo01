<?php
namespace Entities;

class AccessAccount {
    
    private $username;
    private $password;
    private $enabled;

    public function __construct($username, $password, $enabled) {
        $this->username = $username;
        $this->password = $password;
        $this->enabled = $enabled;
    }

    public function getUsername(){
        return $this->username;
    }

}