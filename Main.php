<?php
namespace Main;
use \Entities\AccessAccount;
require('./Entities/AccessAccount.php');

class main{
    public function run(){
        echo "Main!\n";
        $account = new AccessAccount("danielbg", 'hola123.', true);
        printf("Username: [%s] ", $account->getUsername());
        echo "\n";
    }
}

// Ejecucion del principal
//$main = new \Modelo1\Main\Main();
$main = new Main();
$main->run();